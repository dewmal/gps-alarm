package org.duineth.mobile.gps.alarm.cont;

import org.duineth.mobile.gps.alarm.res.Observers;

public interface ObserverSubject {

	public abstract void addObservers(Observers observers);
	
	public abstract void removeObservers(Observers observers);
	
	

}